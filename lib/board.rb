class Board

attr_reader :grid, :marks

def self.blank_grid
  Array.new(3) { Array.new(3) }
end

def initialize(grid = Board.blank_grid)
  @grid = grid
  @marks = [:X, :O]
end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    return true if self[pos].nil?
    false
  end

  def row_win?(mark)
    return true if @grid[0].all? {|el| el == mark}
    return true if @grid[1].all? {|el| el == mark}
    return true if @grid[2].all? {|el| el == mark}
    false
  end

  def ldiagonal_win?(mark)
    if @grid[0][0] == mark && @grid[1][1] == mark && @grid[2][2] == mark
      true
    else
      false
    end
  end

  def rdiagonal_win?(mark)
    if @grid[0][2] == mark && @grid[1][1] == mark && @grid[2][0] == mark
      true
    else
      false
    end
  end

  def column_win?(mark)
    cols = @grid.transpose
    return true if cols[2].all? {|el| el == mark}
    return true if cols[1].all? {|el| el == mark}
    return true if cols[0].all? {|el| el == mark}
    false
  end

  def winner
    if row_win?(:X) || ldiagonal_win?(:X) || rdiagonal_win?(:X) || column_win?(:X)
      :X
    elsif row_win?(:O) || ldiagonal_win?(:O) || rdiagonal_win?(:O) || column_win?(:O)
      :O
    else
      nil
    end
  end

  def over?
    winner || !winner && grid.flatten.count(:X)>= 4
  end

end
