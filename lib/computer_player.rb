class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    open_moves = []
    (0..2).each do |idx1|
      (0..2).each do |idx2|
        pos = [idx1, idx2]
        open_moves << pos if board[pos].nil?
      end
    end

    open_moves.each do |move|
      return move if winning_move?(move)
    end

    open_moves.shuffle.first
  end

  def winning_move?(move)
    board[move] = mark
    if board.winner == mark
      board[move] = nil
      true
    else
      board[move] = nil
      false
    end
  end

end
