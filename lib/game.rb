require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :current_player, :player_one, :player_two, :board

  def initialize(player_one, player_two)
    @player_one, @player_two = player_one, player_two
    player_one.mark = :X
    player_two.mark = :O
    @current_player = player_one
    @board = Board.new
  end

  def switch_players!
    self.current_player = current_player == player_one ? player_two : player_one
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def play
    puts "WELCOME TO TIC-TAC-TOE #{player_one.name.upcase}!"
    puts "May the smartest player win!"
    current_player.display(board)
    play_turn until board.over?

    if game_winner
      game_winner.display(board)
      puts "#{game_winner.name} wins!"
    else
      puts "It's a tie!"
    end
  end

  def game_winner
    return player_one if board.winner == player_one.mark
    return player_two if board.winner == player_two.mark
    nil
  end

end

if $PROGRAM_NAME == __FILE__
  print "Enter your name: "
  name = gets.chomp.strip
  human = HumanPlayer.new(name)
  garry = ComputerPlayer.new('garry')

  new_game = Game.new(human, garry)
  new_game.play
end
